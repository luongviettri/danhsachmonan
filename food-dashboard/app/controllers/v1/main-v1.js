import { MonAnV1 } from "../../models/v1/monAnModel-v1.js";
function layThongTinTuForm() {
    let ma = document.getElementById("foodID").value;
    let ten = document.getElementById("tenMon").value;
    let loai = document.getElementById("loai").value;
    let gia = document.getElementById("giaMon").value;
    let khuyenMai = document.getElementById("khuyenMai").value;
    let tinhTrang = document.getElementById("tinhTrang").value;
    let hinhMon = document.getElementById("hinhMon").value;
    let moTa = document.getElementById("moTa").value;
    let monAn = new MonAnV1(ma, ten, loai, gia, khuyenMai, tinhTrang, hinhMon, moTa)
    return monAn;
}

let showMonAnLenForm = (monAn) => {
    let { ma, ten, loai, gia, phanTramKm, tinhTrang, hinhMon, Mota } = monAn;
    // ! hình
    document.getElementById("imgMonAn").src = hinhMon;
    document.getElementById("spMa").innerText = ma;
    document.getElementById("spTenMon").innerText = ten;
    document.getElementById("spLoaiMon").innerText = loai;
    // ! giá món
    document.getElementById("spGia").innerText = gia;
    // ! tình trạng
    document.getElementById("spTT").innerText = tinhTrang;
    // ! mô tả
    document.getElementById("pMoTa").innerText = Mota;

    // FIXME: khuyến mãi với giá khuyến mãi khác nhau chỗ nào
    //  khuyến mãi
    document.getElementById("spKM").innerText = phanTramKm;

    //  giá khuyến mãi
    document.getElementById("spGiaKM").innerText = monAn.tinhGiaKM();

}

document.getElementById('btnThemMon').addEventListener('click', function () {
    let monAn = layThongTinTuForm();
    showMonAnLenForm(monAn);
})