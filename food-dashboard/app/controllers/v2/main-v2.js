import { layThongTinTuForm, renderTable, showMonAnLenForm } from './controller-v2.js'

let mangMonAn = [];
document.getElementById("btnThemMon").addEventListener('click', () => {
    let monAn = layThongTinTuForm();
    // ! add đối tượng vào mảng món ăn, render table mảng món ăn
    mangMonAn.push(monAn);
    // !render ra bảng
    renderTable(mangMonAn);
})

const xoaMonAn = (ma) => {
    let mangMoi = [...mangMonAn];
    mangMoi = mangMonAn.filter((monAn) => {
        return monAn.ma != ma;
    })
    renderTable(mangMoi);
}
window.xoaMonAn = xoaMonAn;
const suaMonAn = (ma) => {
    let monAnCanSua = mangMonAn.find((monAn) => {
        return monAn.ma == ma;
    })
    showMonAnLenForm(monAnCanSua);
}
window.suaMonAn = suaMonAn;
