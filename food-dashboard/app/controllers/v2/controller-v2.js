import { MonAnV2 } from "../../models/v2/monAnModel-v2.js";
export function layThongTinTuForm() {
    //! ma, ten, loai, gia, khuyenMai, tinhTrang, hinhMon, Mota
    let ma = document.getElementById("foodID").value;
    let ten = document.getElementById("tenMon").value;
    let loai = document.getElementById("loai").value;
    let gia = document.getElementById("giaMon").value;
    let khuyenMai = document.getElementById("khuyenMai").value;
    let tinhTrang = document.getElementById("tinhTrang").value;
    let hinhMon = document.getElementById("hinhMon").value;
    let Mota = document.getElementById("moTa").value;
    let monAn = new MonAnV2(ma, ten, loai, gia, khuyenMai, tinhTrang, hinhMon, Mota);
    return monAn;
}
/**
 * hàm renderTable nhận vào mảng đối tượng và render mảng thành table
 */
export function renderTable(mangMonAn) {

    let myTbody = document.getElementById('tbodyFood');
    var rows = ``;

    mangMonAn.forEach((monAn) => {
        let { ma, ten, loai, gia, khuyenMai, tinhTrang, hinhMon, Mota } = monAn;
        let row =
            `
        <tr>
            <td>${ma}</td>
            <td>${ten}</td>
            <td>${loai == "chay" ? "Chay" : "Mặn"}</td>
            <td>${gia}</td>
            <td>${khuyenMai * 100}%</td>
            <td>${monAn.tinhGiaKM()}</td>
            <td>${tinhTrang == "0" ? "Hết" : "Còn"}</td>
            <td>
            <button
            data-toggle="modal" data-target="#exampleModal"
            onclick= "suaMonAn(${ma})" 
            class="btn btn-warning" >Sửa </button>
            <button
            class="btn btn-danger" onclick= "xoaMonAn(${ma})" >Xóa </button>
            </td>
        </tr>
        `
        rows += row;
    })
    myTbody.innerHTML = rows;
}
export const showMonAnLenForm = (monAnCanSua) => {
    let { ma, ten, loai, gia, khuyenMai, tinhTrang, hinhMon, Mota } = monAnCanSua
    document.getElementById("foodID").value = ma;
    document.getElementById("tenMon").value = ten;
    document.getElementById("loai").value = loai;
    document.getElementById("giaMon").value = gia;
    document.getElementById("khuyenMai").value = khuyenMai;
    document.getElementById("tinhTrang").value = tinhTrang;
    document.getElementById("hinhMon").value = hinhMon;
    document.getElementById("moTa").value = Mota;
}