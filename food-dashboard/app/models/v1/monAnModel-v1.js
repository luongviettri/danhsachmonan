export class MonAnV1 {
    constructor(ma, ten, loai, gia, phanTramKm, tinhTrang, hinhMon, Mota) {
        this.ma = ma;
        this.ten = ten;
        this.loai = loai;
        this.gia = gia;
        this.phanTramKm = phanTramKm;
        this.tinhTrang = tinhTrang;
        this.hinhMon = hinhMon;
        this.Mota = Mota;
    }
    tinhGiaKM = function () {
        return this.gia * (1 - this.phanTramKm);
    }
}