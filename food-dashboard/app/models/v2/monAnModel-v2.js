export class MonAnV2 {
    constructor(ma, ten, loai, gia, khuyenMai, tinhTrang, hinhMon, Mota) {
        this.ma = ma;
        this.ten = ten;
        this.loai = loai;
        this.gia = gia;
        this.khuyenMai = khuyenMai;
        this.tinhTrang = tinhTrang;
        this.hinhMon = hinhMon;
        this.Mota = Mota;
    }
    tinhGiaKM = function () {
        return this.gia * (1 - this.khuyenMai);
    }
}